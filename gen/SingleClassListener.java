import Classes.*;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by A. Goldani on 2019-06-30.
 * Student ID: 9512762107
 * compiler_project_semantic_analysis
 */

public class SingleClassListener extends JythonBaseListener {
    private Scope scope;

    @Override
    public void exitClassDec(JythonParser.ClassDecContext ctx) {

        for (Scope childScope : Translate.rootScope.getChildrenScope())
        {
            if (childScope.getName().equals(ctx.className.getText()))
                scope = childScope;
        }

        //checking for not resolved method calls and variables in recursive iteration
        resolveNotDefinedMethods(scope);
        resolveAlreadyDefinedMethods(scope);
        resolveAlreadyDefinedVars(scope);
        resolveNotDefinedVars(scope);
    }
    //Error 105
    private void resolveNotDefinedMethods(Scope scope){
        Scope tempScope;
        boolean notDef;
        if(scope.getChildrenScope() == null) return;
        for(int i = 0; i<scope.getChildrenScope().size();i++){
            if(scope.getChildrenScope().get(i).getName() != null) continue;
            for (Map.Entry<String, Symbol> entry : scope.getChildrenScope().get(i).getSymbolTable().getEntries().entrySet()) {
                notDef = true;
                tempScope = scope;
                if (entry.getValue() instanceof JythonMethod && !entry.getValue().isResolved()) {
                    while (!tempScope.equals(Translate.rootScope)) {
                        //goes to the classDec scope to look for methodDec
                        //if scope is JythonClass
                        if (tempScope.getName() != null) {
                            for (Map.Entry<String, Symbol> entry1 : tempScope.getSymbolTable().getEntries().entrySet()) {
                                Matcher m = Pattern.compile(entry.getKey().replaceAll("@([\\w+]*)", "")).matcher((entry1.getKey().replaceAll("@([\\w+]*)", "")));

                                //similar methodNames means it's already been defined
                                if (entry1.getValue() instanceof JythonMethod &&
                                        m.matches() && ((JythonMethod) entry.getValue()).getSizeOfParameters() == ((JythonMethod) entry1.getValue()).getParameters().size()) {
                                    notDef = false;
                                    break;
                                }
                            }
                            //break;
                        }
                        tempScope = tempScope.getParentScope();
                    }
                    if (notDef) {
                        System.out.println("Error 105: in line " + entry.getValue().getDefLine() +
                                ", can not find method " + entry.getKey().replaceAll("@([\\w+]*)", ""));
                    }
                }

            }


            //check for all the children
            resolveNotDefinedMethods(scope.getChildrenScope().get(i));
        }
    }

    //Error 108
    private void resolveNotDefinedVars(Scope scope){
        boolean notDef;
        Scope tempScope;
        if(scope.getChildrenScope() == null) return;
        for(int i = 0;i<scope.getChildrenScope().size();i++){
            if(scope.getChildrenScope().get(i).getName() != null) continue;
            for (Map.Entry<String, Symbol> entry : scope.getChildrenScope().get(i).getSymbolTable().getEntries().entrySet()) {
                notDef = true;
                tempScope = scope.getChildrenScope().get(i);
                if(entry.getValue() instanceof JythonVariable && !entry.getValue().isResolved() && entry.getKey().contains("@@")){
                    while (!tempScope.equals(Translate.rootScope)) {
                        //goes to the classDec scope to look for methodDec
                        //if scope is JythonClass

                        for (Map.Entry<String, Symbol> entry1 : tempScope.getSymbolTable().getEntries().entrySet()) {
                            //Matcher m = Pattern.compile(entry.getKey().replaceAll("@([\\w+]*)", "")).matcher((entry1.getKey().replaceAll("@([\\w+]*)", "")));

                            //similar methodNames means it's already been defined
                            if (entry1.getValue() instanceof JythonVariable &&
                                    entry1.getValue().getName().equals(entry.getValue().getName())) {
                                if(entry1.getKey().contains("@@")) continue;
                                notDef = false;
                                break;
                            }
                        }
                        if(!notDef) break;
                        tempScope = tempScope.getParentScope();
                    }
                    if (notDef) {
                        System.out.println("Error 108: in line " + entry.getValue().getDefLine() +
                                ", can not find Variable " + entry.getValue().getName());
                    }

                }
            }
            //check for all the children
            resolveNotDefinedVars(scope.getChildrenScope().get(i));

        }


    }

    //Error 102
    private void resolveAlreadyDefinedVars(Scope scope){
        boolean alreadyDef;
        Scope tempScope;
        for (Map.Entry<String, Symbol> entry : scope.getSymbolTable().getEntries().entrySet()) {
            alreadyDef = false;
            tempScope = scope;
            if (entry.getValue() instanceof JythonVariable && !entry.getValue().isResolved() && entry.getKey().contains("@")) {
                while (!tempScope.getParentScope().equals(Translate.rootScope)) {
                        for (Map.Entry<String, Symbol> entry1 : tempScope.getParentScope().getSymbolTable().getEntries().entrySet()) {
                            if (entry1.getValue() instanceof JythonVariable && entry1.getValue().getName().equals(entry.getValue().getName())) {
                                alreadyDef = true;
                                break;
                            }
                        }

                    tempScope = tempScope.getParentScope();
                }
                if (alreadyDef) {
                    System.out.println("Error102: in line " + entry.getValue().getDefLine() + ", " + entry.getValue().getName() +
                            " has been defined already in " +
                            tempScope.getName());
                }
            }
        }

    }

    //Error 102
    private void resolveAlreadyDefinedMethods(Scope scope){
        boolean alreadyDef;
        boolean flag;
        Scope tempScope;

        for (Map.Entry<String, Symbol> entry : scope.getSymbolTable().getEntries().entrySet()) {
            Matcher m1 = Pattern.compile("main@([\\w+]*)").matcher(entry.getKey());
            if(m1.matches()) continue;
            alreadyDef = false;
            tempScope = scope;
            if(entry.getValue() instanceof JythonMethod && !entry.getValue().isResolved()){
                while(!tempScope.getParentScope().equals(Translate.rootScope)){
                    for (Map.Entry<String, Symbol> entry1 : tempScope.getParentScope().getSymbolTable().getEntries().entrySet()) {
                        Matcher m = Pattern.compile(entry.getKey().replaceAll("@([\\w+]*)", "")).matcher((entry1.getKey().replaceAll("@([\\w+]*)", "")));
                        flag = true;
                        if (entry1.getValue() instanceof JythonMethod
                               && m.matches() && ((JythonMethod) entry.getValue()).getParameters().size() == ((JythonMethod) entry1.getValue()).getParameters().size()) {
                            for(int i = 0;i<((JythonMethod) entry.getValue()).getParameters().size();i++){
                                if(!(((JythonMethod) entry.getValue()).getParameters().get(i).getType().equals(((JythonMethod) entry1.getValue()).getParameters().get(i).getType()))){
                                    flag = false;
                                    break;
                                }
                            }
                            if(flag) {
                                alreadyDef = true;
                                break;
                            }
                        }
                    }
                    tempScope = tempScope.getParentScope();
                }
                if (alreadyDef) {
                    System.out.println("Error102: in line " + entry.getValue().getDefLine() + ", " + entry.getKey().replaceAll("@([\\w+]*)", "") +
                            " has been defined already in " +
                            tempScope.getName());
                }
            }
        }

    }

}