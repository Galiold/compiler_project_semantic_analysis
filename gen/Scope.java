import java.util.ArrayList;

/**
 * Created by A. Goldani on 2019-05-17.
 * Student ID: 9512762107
 * compiler_project_semantic_analysis
 */

public class Scope {

    private String name = null;
    private Scope parentScope = null;
    private ArrayList<Scope> childrenScope = null;
    private SymbolTable symbolTable;

    public Scope(Scope parentScope) {
        this.symbolTable = new SymbolTable();
        this.parentScope = parentScope;
        //setChildScope(this);
    }

    public Scope() {
        this.symbolTable = new SymbolTable();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Scope getParentScope() {
        return parentScope;
    }

    public void setParentScope(Scope parentScope) {
        this.parentScope = parentScope;
    }

    public SymbolTable getSymbolTable() {
        return symbolTable;
    }

    public void setSymbolTable(SymbolTable symbolTable) {
        this.symbolTable = symbolTable;
    }


    public ArrayList<Scope> getChildrenScope() {
        return childrenScope;
    }

    public void addToChildrenScope(Scope childScope) {
        if(this.childrenScope == null){
            this.childrenScope = new ArrayList<>();
        }

        this.childrenScope.add(childScope);
    }
}
