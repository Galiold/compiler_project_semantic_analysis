import Classes.Symbol;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by A. Goldani on 2019-05-17.
 * Student ID: 9512762107
 * compiler_project_semantic_analysis
 */

public class SymbolTable {
    private Map<String, Symbol> entries = new HashMap<>();

    public Map<String, Symbol> getEntries() {
        return entries;
    }

    public void setEntries(Map<String, Symbol> entries) {
        this.entries = entries;
    }
}
