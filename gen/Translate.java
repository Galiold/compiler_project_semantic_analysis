import Classes.*;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;

public class Translate
{
    public static Scope rootScope = new Scope();

    public static void main(String[] args) throws Exception {
        final String inputFilePath = "samples/Human.jy";
        File inputDirectory = new File(inputFilePath).getParentFile();

        //traversing all classes in folder
        for (File fileEntry : Objects.requireNonNull(inputDirectory.listFiles())) {

            // CharStream that reads from standard input
            CharStream stream = CharStreams.fromFileName(fileEntry.getPath());
            // Lexer that feeds off of input CharStream
            JythonLexer lexer = new JythonLexer(stream);
            // ‌Buffer of tokens pulled from the lexer
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            // Parser that feeds off the tokens buffer
            JythonParser parser = new JythonParser(tokens);
            // Begin parsing at program rule - the start rule
            ParseTree tree = parser.program();
            // A generic parse tree walker that can trigger callbacks
            ParseTreeWalker walker = new ParseTreeWalker();
            // Create a custom listener
            ClassListener classListener = new ClassListener();
            // Walk the tree created during the parse, trigger callbacks
            walker.walk(classListener, tree);

        }

        resolveInheritance();

        if (detectInheritanceLoops()){
            System.out.println("\n<Please Resolve Invalid Inheritance to Proceed>");
            return;
        }

        for (File fileEntry : Objects.requireNonNull(inputDirectory.listFiles()))
        {

            System.out.println("================= " + fileEntry.getName() + ": Walk initiated =================");

            // CharStream that reads from standard input
            CharStream stream = CharStreams.fromFileName(fileEntry.getPath());
            // Lexer that feeds off of input CharStream
            JythonLexer lexer = new JythonLexer(stream);
            // ‌Buffer of tokens pulled from the lexer
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            // Parser that feeds off the tokens buffer
            JythonParser parser = new JythonParser(tokens);
            // Begin parsing at program rule - the start rule
            ParseTree tree = parser.program();
            // A generic parse tree walker that can trigger callbacks
            ParseTreeWalker walker = new ParseTreeWalker();
            // Create a custom listener
            CustomListener customListener = new CustomListener();
            // Walk the tree created during the parse, trigger callbacks
            walker.walk(customListener, tree);

            System.out.println("================= " + fileEntry.getName() + ": Walk concluded =================\n");

        }

            //checking for not resolved variables
        for (File fileEntry : Objects.requireNonNull(inputDirectory.listFiles()))
        {

            System.out.println("======checking resolved vars=========== " + fileEntry.getName() + ": Walk initiated =================");
            // CharStream that reads from standard input
            CharStream stream = CharStreams.fromFileName(fileEntry.getPath());
            // Lexer that feeds off of input CharStream
            JythonLexer lexer = new JythonLexer(stream);
            // ‌Buffer of tokens pulled from the lexer
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            // Parser that feeds off the tokens buffer
            JythonParser parser = new JythonParser(tokens);
            // Begin parsing at program rule - the start rule
            ParseTree tree = parser.program();
            // A generic parse tree walker that can trigger callbacks
            ParseTreeWalker walker = new ParseTreeWalker();
            // Create a custom listener
            SingleClassListener singleClassListener = new SingleClassListener();
            // Walk the tree created during the parse, trigger callbacks
            walker.walk(singleClassListener, tree);

            System.out.println("=======checking resolved vars========== " + fileEntry.getName() + ": Walk concluded =================\n");

        }


    }

    private static void resolveInheritance(){
        for (Map.Entry<String, Symbol> classEntry : rootScope.getSymbolTable().getEntries().entrySet())
        {
            if (!((JythonClass) classEntry.getValue()).getSuperClassName().equals("Symbol"))
            {
                for (Map.Entry<String, Symbol> superClassEntry : rootScope.getSymbolTable().getEntries().entrySet())
                {
                    if (((JythonClass) classEntry.getValue()).getSuperClassName().equals(((JythonClass) superClassEntry.getValue()).getName()))
                    {
                        ((JythonClass)classEntry.getValue()).setSuperClass(((JythonClass) superClassEntry.getValue()));
                    }
                }
            }
        }
    }


    //finding Error 107
    private static boolean detectInheritanceLoops()
    {
        boolean invalidInheritance = false;
        ArrayList<String> classHierarchy = new ArrayList<>();
        for (Map.Entry<String, Symbol> classEntry : rootScope.getSymbolTable().getEntries().entrySet())
        {
            classHierarchy.clear();
            JythonClass tempClass = (JythonClass) classEntry.getValue();
            if (tempClass.getSuperClass() != null)
            {

                while (tempClass.getSuperClass() != null)
                {
                    if (classHierarchy.contains(tempClass.getName()))
                    {
                        System.out.print("Error107: Invalid inheritance ");
                        for (int i = classHierarchy.indexOf(tempClass.getName()); i < classHierarchy.size(); i++)
                        {
                            System.out.print(classHierarchy.get(i) + " -> ");
                        }
                        System.out.println(tempClass.getName());
                        invalidInheritance = true;
                        break;
                    }
                    classHierarchy.add(tempClass.getName());
                    tempClass = tempClass.getSuperClass();
                }
            }
        }
        return invalidInheritance;
    }
}