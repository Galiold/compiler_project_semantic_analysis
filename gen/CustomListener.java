import Classes.*;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class CustomListener extends JythonBaseListener {
    private Scope scope;
    //to save imported Classes for further checking used classes
    private ArrayList<String> importedClasses = new ArrayList<>();
    private final String[] jythonTypes = {"float", "int", "bool", "string", "void"};

    @Override
    public void enterProgram(JythonParser.ProgramContext ctx) {
        super.enterProgram(ctx);
    }

    @Override
    public void exitProgram(JythonParser.ProgramContext ctx) {
        super.exitProgram(ctx);
    }

    @Override
    public void enterImportClass(JythonParser.ImportClassContext ctx) {
        super.enterImportClass(ctx);
        boolean doesExist = false;
        for (Scope childScope : Translate.rootScope.getChildrenScope()) {
            if (childScope.getName().equals(ctx.children.get(1).getText()))
                doesExist = true;
        }
        //if the class hasn't defined in any of given files
        if (!doesExist)
            System.out.println("Error106: in line " + ctx.start.getLine() + " ,cannot find class " + ctx.children.get(1).getText());
            //if the class has defined then importing is write and now we can use the class
        else importedClasses.add(ctx.children.get(1).getText());
    }

    @Override
    public void exitImportClass(JythonParser.ImportClassContext ctx) {
        super.exitImportClass(ctx);
    }

    @Override
    public void enterClassDec(JythonParser.ClassDecContext ctx) {

        for (Scope childScope : Translate.rootScope.getChildrenScope()) {
            if (childScope.getName().equals(ctx.className.getText()))
                scope = childScope;
        }
        JythonClass newClass;
        if (ctx.superClassName != null) {
            for (Scope childScope : Translate.rootScope.getChildrenScope()) {
                if (childScope.getName().equals(ctx.superClassName.getText())) {
                    scope.setParentScope(childScope);
                    childScope.addToChildrenScope(scope);

                }

            }
            newClass = new JythonClass(ctx.className.getText(), ctx.superClassName.getText(), Kind.CLASS, ctx.start.getLine());
        } else {
            newClass = new JythonClass(ctx.className.getText(), Kind.CLASS, ctx.start.getLine());
        }

    }

    @Override
    public void exitClassDec(JythonParser.ClassDecContext ctx) {
        //Error 104
        boolean isMain = false;
        for (Map.Entry<String, Symbol> entry : scope.getSymbolTable().getEntries().entrySet()) {
            Matcher m = Pattern.compile("main@([\\w+]*)").matcher(entry.getKey());
            if (m.matches()) {
                if (entry.getValue() instanceof JythonMethod
                        && ((JythonMethod) entry.getValue()).getReturnType().equals("void")
                        && ((JythonMethod) entry.getValue()).getParameters().size() == 0) {
                    isMain = true;
                    break;
                }
            }
        }
        if (!isMain) {
            System.out.println("Error104: in line " + ctx.stop.getLine() + ", Can not find main method");
        }
//
//
//        if (!(scope.getSymbolTable().getEntries().containsKey("main")
//                && scope.getSymbolTable().getEntries().get("main") instanceof JythonMethod
//                && ((JythonMethod) scope.getSymbolTable().getEntries().get("main")).getReturnType().equals("void")
//                && ((JythonMethod) scope.getSymbolTable().getEntries().get("main")).getParameters().size() == 0))
//            System.out.println("Error104: in line " + ctx.stop.getLine() + ", Can not find main method");

        //Error 105
        //checking for not resolved method calls in recursive iteration
        //resolveNotDefinedMethods(scope);
//        scope = scope.getParentScope();
        //System.out.println("exit");
    }

    // this is going to be checked in SingleClassListener
    public void scopeTraversal(Scope scope) {
        Scope tempScope = scope;
        boolean notDef;
        String[] name;
        String[] name1;
        if (scope.getChildrenScope() == null) return;
        for (int i = 0; i < scope.getChildrenScope().size(); i++) {
            for (Map.Entry<String, Symbol> entry : scope.getChildrenScope().get(i).getSymbolTable().getEntries().entrySet()) {
                notDef = true;
                if (entry.getValue() instanceof JythonMethod && !entry.getValue().isResolved()) {
                    //getting the name part because of the format we saved name+"_"+line
//                        name = entry.getKey().split("_");
                    while (!tempScope.getParentScope().equals(Translate.rootScope)) {
                        //goes to the classDec scope to look for methodDec
                        if (tempScope.getParentScope().getSymbolTable().getEntries().values().iterator().next() instanceof JythonClass) {
                            for (Map.Entry<String, Symbol> entry1 : tempScope.getSymbolTable().getEntries().entrySet()) {
                                Matcher m = Pattern.compile(entry.getKey().replaceAll("@([\\w+]*)", "")).matcher((entry1.getKey().replaceAll("@([\\w+]*)", "")));

//                                    name1 = entry1.getKey().split("_");
                                //similar methodNames means it's already been defined
                                if (entry1.getValue() instanceof JythonMethod &&
                                        m.matches() && ((JythonMethod) entry.getValue()).getSizeOfParameters() == ((JythonMethod) entry1.getValue()).getParameters().size()) {
                                    notDef = false;
                                    break;
                                }
                            }
                            break;
                        }
                        tempScope = tempScope.getParentScope();
                    }
                    if (notDef) {
                        System.out.println("Error 105: in line " + entry.getValue().getDefLine() +
                                ", can not find method " + entry.getKey().replaceAll("@([\\w+]*)", ""));
                    }
                }
            }
            //check for all the children
            scopeTraversal(scope.getChildrenScope().get(i));
        }
    }

    @Override
    public void enterClass_body(JythonParser.Class_bodyContext ctx) {
        if (ctx.varDec() != null) {
            AtomicBoolean found = new AtomicBoolean(false);
            scope.getSymbolTable().getEntries().forEach((name, entry) -> {
                if (name.equals(ctx.varDec().varName.getText()) && entry instanceof JythonVariable &&
                        (((JythonVariable) entry).getType().equals(ctx.varDec().type().getText()))) {
                    System.out.println("Error102: in line " + ctx.start.getLine() + ", " + ctx.varDec().varName.getText() +
                            " has been defined already in " +
                            ctx.parent.getChild(1).getText());
                    found.set(true);
                }
            });
            //checking for undefined classes for variable declaration
            boolean isJType = false;
            boolean classExists = false;
            for (String jType : jythonTypes) {
                if (ctx.varDec().type().getText().equals(jType)) isJType = true;
            }
            if (!isJType) {
                for (String type : importedClasses) {
                    if (type.equals(ctx.varDec().type().getText())) {
                        classExists = true;
                    }
                }
            }
            //error 106
            if (!classExists && !isJType)
                System.out.println("Error106: in line " + ctx.start.getLine() + " ,cannot find class " + ctx.varDec().type().getText());

            if (!found.get())
                scope.getSymbolTable().getEntries().put(ctx.varDec().varName.getText() + "@" + ctx.start.getLine(), new JythonVariable(ctx.varDec().varName.getText(), ctx.varDec().type().getText(), ctx.start.getLine(), false));
            else
                scope.getSymbolTable().getEntries().put(ctx.varDec().varName.getText() + "@" + ctx.start.getLine(), new JythonVariable(ctx.varDec().varName.getText(), ctx.varDec().type().getText(), ctx.start.getLine(), true));
        }

        else if (ctx.arrayDec() != null) {
            AtomicBoolean found = new AtomicBoolean(false);
            Scope tempScope = scope;

            while (tempScope.getName() == null) {
                tempScope.getSymbolTable().getEntries().forEach((name, entry) -> {
                    if (name.equals(ctx.arrayDec().arrayName.getText()) && entry instanceof JythonVariable &&
                            (((JythonVariable) entry).getType().equals(ctx.arrayDec().type().getText()))) {
                        System.out.println("Error103: in line " + ctx.start.getLine() + ", " + ctx.arrayDec().arrayName.getText() +
                                " has been defined already in current scope"
                        );
                        found.set(true);
                    }
                });
                tempScope = tempScope.getParentScope();
            }

            //checking for undefined classes for variable declaration
            boolean isJType = false;
            boolean classExists = false;
            for (String jType : jythonTypes) {
                if (ctx.arrayDec().type().getText().equals(jType)) isJType = true;
            }
            if (!isJType) {
                for (String type : importedClasses) {
                    if (type.equals(ctx.arrayDec().type().getText())) {
                        classExists = true;
                    }
                }
            }
            //error 106
            if (!classExists && !isJType)
                System.out.println("Error106: in line " + ctx.start.getLine() + " ,cannot find class " + ctx.arrayDec().type().getText());

            if (!found.get())
                scope.getSymbolTable().getEntries().put(ctx.arrayDec().arrayName.getText() + "@" + ctx.start.getLine(), new JythonVariable(ctx.arrayDec().arrayName.getText(), ctx.arrayDec().type().getText(), ctx.start.getLine(), false));
            else
                scope.getSymbolTable().getEntries().put(ctx.arrayDec().arrayName.getText() + "@" + ctx.start.getLine(), new JythonVariable(ctx.arrayDec().arrayName.getText(), ctx.arrayDec().type().getText(), ctx.start.getLine(), true));


        }

        else if (ctx.methodDec() != null) {
            boolean found = false;
            ArrayList<JythonVariable> parameters = new ArrayList<>();
            Scope methodScope = new Scope(scope);
            scope.addToChildrenScope(methodScope);
            scope = methodScope;

            //checking for undefined classes for method declaration
            boolean isJType = false;
            boolean classExists = false;

            //first the parameters must be checked
            if (ctx.methodDec().parameters().size() != 0) {
                for (int i = 0; i < ctx.methodDec().parameters().get(0).parameter().size(); i++) {
                    isJType = false;
                    classExists = false;
                    for (String jType : jythonTypes) {
                        if (ctx.methodDec().parameters().get(0).parameter(i).varDec().type().getText().equals(jType))
                            isJType = true;
                    }
                    if (!isJType) {
                        for (String type : importedClasses) {
                            if (type.equals(ctx.methodDec().parameters().get(0).parameter(i).varDec().type().getText())) {
                                classExists = true;
                            }
                        }
                    }
                    if (!classExists && !isJType) {
                        //error 106
                        System.out.println("Error106: in line " + ctx.start.getLine() + " ,cannot find class " + ctx.methodDec().parameters().get(0).parameter(i).varDec().type().getText());
                    }
                }
            }
            //checking the return type of the method for undefined classes
            for (String jType : jythonTypes) {
                if (ctx.methodDec().returnType().getText().equals(jType))
                    isJType = true;
            }
            if (!isJType) {
                for (String type : importedClasses) {
                    if (type.equals(ctx.methodDec().returnType().getText())) {
                        classExists = true;
                    }
                }
            }
            //error 106
            if (!classExists && !isJType)
                System.out.println("Error106: in line " + ctx.start.getLine() + " ,cannot find class " + ctx.methodDec().returnType().getText());


            for (Map.Entry<String, Symbol> entry : scope.getParentScope().getSymbolTable().getEntries().entrySet()) {

                Matcher m = Pattern.compile(ctx.methodDec().methonName.getText() + "@" + "([\\w+]*)").matcher(entry.getKey());

                if (entry.getValue() instanceof JythonMethod && m.matches()
                        && ctx.methodDec().parameters().get(0).parameter().size() ==
                        ((JythonMethod) entry.getValue()).getParameters().size()
                        && (ctx.methodDec().returnType().getText().equals
                        (((JythonMethod) entry.getValue()).getReturnType()))
                ) {
                    for (int i = 0; i < ((JythonMethod) entry.getValue()).getParameters().size(); i++) {
                        if (ctx.methodDec().parameters().get(0).parameter(i) != null) {
                            if (!(ctx.methodDec().parameters().get(0).parameter(i).varDec().type().getText().
                                    equals(((JythonMethod) entry.getValue()).getParameters().get(i).getType()))) {
                                break;
                            }
                        }
                    }
                    System.out.println("Error102: in line " + ctx.start.getLine() + ", " + ctx.methodDec().methonName.getText() +
                            " has been defined already in " +
                            ctx.parent.getChild(1).getText());
                    found = true;
                }
            }

            if (ctx.methodDec().parameters().size() != 0) {
                for (int i = 0; i < ctx.methodDec().parameters().get(0).parameter().size(); i++) {
                    parameters.add(new JythonVariable(ctx.methodDec().parameters().get(0).parameter(i).varDec().varName.getText(),
                            ctx.methodDec().parameters().get(0).parameter(i).varDec().type().getText(),
                            ctx.start.getLine()));
                }
            }
            if (!found) {
                scope.getParentScope().getSymbolTable().getEntries().put(ctx.methodDec().methonName.getText() + "@" + ctx.start.getLine(), new JythonMethod(parameters, ctx.methodDec().returnType().getText(), ctx.start.getLine(), false));
                for (int i = 0; i < parameters.size(); i++) {
                    scope.getSymbolTable().getEntries().put(parameters.get(i).getName(), parameters.get(i));
                }
            } else
                scope.getParentScope().getSymbolTable().getEntries().put(ctx.methodDec().methonName.getText() + "@" + ctx.start.getLine(),
                        new JythonMethod(parameters, ctx.methodDec().returnType().getText(), ctx.start.getLine(), true));

        }

    }

    @Override
    public void exitClass_body(JythonParser.Class_bodyContext ctx) {

    }

    @Override
    public void enterVarDec(JythonParser.VarDecContext ctx) {

    }

    @Override
    public void exitVarDec(JythonParser.VarDecContext ctx) {
        super.exitVarDec(ctx);
    }

    @Override
    public void enterArrayDec(JythonParser.ArrayDecContext ctx) {
        super.enterArrayDec(ctx);
    }

    @Override
    public void exitArrayDec(JythonParser.ArrayDecContext ctx) {
        super.exitArrayDec(ctx);
    }

    @Override
    public void enterMethodDec(JythonParser.MethodDecContext ctx) {
        // Error 230

        String methodReturnType = ctx.returnType().getText();


        // method has no return value
        if (methodReturnType.equals("void")) {
            for (JythonParser.StatmentContext st : ctx.statment()) {
                if (st.return_statment() != null) {
                    System.out.println("Error230: in line " + st.start.getLine() + ", ReturnType of this method must be " + methodReturnType);
                }
            }
        }

        //  method has return value
        else {
            boolean hasReturnValue = false;
            boolean outputTypeMatches = false;

            for (JythonParser.StatmentContext st : ctx.statment()) {

                if (st.return_statment() != null) {
                    hasReturnValue = true;

                    if (st.return_statment().expression().rightExp() != null) {
                        if (methodReturnType.equals("int")) {
                            if (st.return_statment().expression().rightExp().INTEGER() != null) {
                                outputTypeMatches = true;
                            }
                        }
                        if (methodReturnType.equals("float")) {
                            if (st.return_statment().expression().rightExp().FLOAT() != null) {
                                outputTypeMatches = true;
                            }
                        }
                        if (methodReturnType.equals("string")) {
                            if (st.return_statment().expression().rightExp().STRING() != null) {
                                outputTypeMatches = true;
                            }
                        }
                        if (methodReturnType.equals("bool")) {
                            if (st.return_statment().expression().rightExp().bool() != null) {
                                outputTypeMatches = true;
                            }
                        }

                    }


                    if (methodReturnType.equals("bool") && (st.return_statment().expression().eq_neq() != null || st.return_statment().expression().relation_operators() != null)) {
                        outputTypeMatches = true;
                    } else if ((methodReturnType.equals("int") || methodReturnType.equals("float")) &&
                            (st.return_statment().expression().add_sub() != null || st.return_statment().expression().mult_mod_div() != null)) {
                        outputTypeMatches = true;
                    } else if (st.return_statment().expression().rightExp().leftExp() != null && st.return_statment().expression().rightExp().leftExp().varName != null) {
                        Scope tempScope = scope;
                        while (tempScope.getParentScope() != null && !outputTypeMatches) {

                            for (Map.Entry<String, Symbol> entry : tempScope.getSymbolTable().getEntries().entrySet()) {
                                Matcher m = Pattern.compile(st.return_statment().expression().rightExp().leftExp().ID().getText()).matcher(entry.getKey());

                                if (m.matches() &&
                                        entry.getValue() instanceof JythonVariable &&
                                        ((JythonVariable) entry.getValue()).getType().equals(methodReturnType)) {
                                    outputTypeMatches = true;
                                    break;
                                }
                            }
                            tempScope = tempScope.getParentScope();
                        }
                    } else if (st.return_statment().expression().rightExp().leftExp() != null &&
                            st.return_statment().expression().rightExp().leftExp().methodName != null) {

                        String methodName = st.return_statment().expression().rightExp().leftExp().methodName.getText();
                        int methodArgsSize = st.return_statment().expression().rightExp().leftExp().args() != null ? 0 :
                                st.return_statment().expression().rightExp().leftExp().args().explist().expression().size();

                        Scope tempScope = scope;

                        while (tempScope.getParentScope() != null && !outputTypeMatches) {

                            for (Map.Entry<String, Symbol> entry : tempScope.getSymbolTable().getEntries().entrySet()) {
                                Matcher m = Pattern.compile(methodName + "@" + "([\\w+]*)").matcher(entry.getKey());

                                if (m.matches() &&
                                        entry.getValue() instanceof JythonMethod &&
                                        ((JythonMethod) entry.getValue()).getReturnType().equals(methodReturnType) &&
                                        ((JythonMethod) entry.getValue()).getParameters().size() == methodArgsSize) {
                                    outputTypeMatches = true;
                                }
                            }
                            tempScope = tempScope.getParentScope();
                        }
                    }

                    if (!outputTypeMatches) {
                        System.out.println("Error230: in line " + st.start.getLine() + ", ReturnType of this method must be " + methodReturnType);
                        break;
                    }
                }


            }
            if (!hasReturnValue) {
                System.out.println("Error230: in line " + ctx.start.getLine() + ", ReturnType of this method must be " + methodReturnType);
            }
        }
    }

    @Override
    public void exitMethodDec(JythonParser.MethodDecContext ctx) {
        scope = scope.getParentScope();
    }

    @Override
    public void enterConstructor(JythonParser.ConstructorContext ctx) {
        super.enterConstructor(ctx);
    }

    @Override
    public void exitConstructor(JythonParser.ConstructorContext ctx) {
        super.exitConstructor(ctx);
    }

    @Override
    public void enterParameter(JythonParser.ParameterContext ctx) {
        super.enterParameter(ctx);
    }

    @Override
    public void exitParameter(JythonParser.ParameterContext ctx) {
        super.exitParameter(ctx);
    }

    @Override
    public void enterParameters(JythonParser.ParametersContext ctx) {
        super.enterParameters(ctx);
    }

    @Override
    public void exitParameters(JythonParser.ParametersContext ctx) {
        super.exitParameters(ctx);
    }

    @Override
    public void enterStatment(JythonParser.StatmentContext ctx) {
        if (ctx.varDec() != null) {
            AtomicBoolean found = new AtomicBoolean(false);
            Scope tempScope = scope;

            while (tempScope.getName() == null) {
                tempScope.getSymbolTable().getEntries().forEach((name, entry) -> {
                    if (name.equals(ctx.varDec().varName.getText()) && entry instanceof JythonVariable &&
                            (((JythonVariable) entry).getType().equals(ctx.varDec().type().getText()))) {
                        System.out.println("Error103: in line " + ctx.start.getLine() + ", " + ctx.varDec().varName.getText() +
                                " has been defined already in current scope"
                        );
                        found.set(true);
                    }
                });
                tempScope = tempScope.getParentScope();

            }

            //checking for undefined classes for variable declaration
            boolean isJType = false;
            boolean classExists = false;
            for (String jType : jythonTypes) {
                if (ctx.varDec().type().getText().equals(jType)) isJType = true;
            }
            if (!isJType) {
                for (String type : importedClasses) {
                    if (type.equals(ctx.varDec().type().getText())) {
                        classExists = true;
                    }
                }
            }
            //error 106
            if (!classExists && !isJType)
                System.out.println("Error106: in line " + ctx.start.getLine() + " ,cannot find class " + ctx.varDec().type().getText());

            if (!found.get())
                scope.getSymbolTable().getEntries().put(ctx.varDec().varName.getText() + "@" + ctx.start.getLine(), new JythonVariable(ctx.varDec().varName.getText(), ctx.varDec().type().getText(), ctx.start.getLine(), false));
            else
                scope.getSymbolTable().getEntries().put(ctx.varDec().varName.getText() + "@" + ctx.start.getLine(), new JythonVariable(ctx.varDec().varName.getText(), ctx.varDec().type().getText(), ctx.start.getLine(), true));

        } else if (ctx.arrayDec() != null) {
            AtomicBoolean found = new AtomicBoolean(false);
            Scope tempScope = scope;

            while (tempScope.getName() == null) {
                tempScope.getSymbolTable().getEntries().forEach((name, entry) -> {
                    if (name.equals(ctx.arrayDec().arrayName.getText()) && entry instanceof JythonVariable &&
                            (((JythonVariable) entry).getType().equals(ctx.arrayDec().type().getText()))) {
                        System.out.println("Error103: in line " + ctx.start.getLine() + ", " + ctx.arrayDec().arrayName.getText() +
                                " has been defined already in current scope"
                        );
                        found.set(true);
                    }
                });
                tempScope = tempScope.getParentScope();
            }

            //checking for undefined classes for variable declaration
            boolean isJType = false;
            boolean classExists = false;
            for (String jType : jythonTypes) {
                if (ctx.arrayDec().type().getText().equals(jType)) isJType = true;
            }
            if (!isJType) {
                for (String type : importedClasses) {
                    if (type.equals(ctx.arrayDec().type().getText())) {
                        classExists = true;
                    }
                }
            }
            //error 106
            if (!classExists && !isJType)
                System.out.println("Error106: in line " + ctx.start.getLine() + " ,cannot find class " + ctx.arrayDec().type().getText());

            if (!found.get())
                scope.getSymbolTable().getEntries().put(ctx.arrayDec().arrayName.getText() + "@" + ctx.start.getLine(), new JythonVariable(ctx.arrayDec().arrayName.getText(), ctx.arrayDec().type().getText(), ctx.start.getLine(), false));
            else
                scope.getSymbolTable().getEntries().put(ctx.arrayDec().arrayName.getText() + "@" + ctx.start.getLine(), new JythonVariable(ctx.arrayDec().arrayName.getText(), ctx.arrayDec().type().getText(), ctx.start.getLine(), true));

        }

        //Error 106
        else if (ctx.assignment() != null) {
            String type = ctx.assignment().children.get(2).getText();
            String[] temp;
            if (type.charAt(0) >= 'A' && type.charAt(0) <= 'Z') {
                temp = type.split("\\(");
                boolean isJType = false;
                boolean classExists = false;
                for (String jType : jythonTypes) {
                    if (temp[0].equals(jType)) isJType = true;
                }
                if (!isJType) {
                    for (String type1 : importedClasses) {
                        if (type1.equals(temp[0])) {
                            classExists = true;
                        }
                    }
                }
                //error 106
                if (!classExists && !isJType)
                    System.out.println("Error106: in line " + ctx.start.getLine() + " ,cannot find class " + temp[0]);
            }

        }

        //Error 105 added
        else if (ctx.method_call() != null) {
            boolean found = false;
            Scope tempScope = scope;
            while (!tempScope.getParentScope().equals(Translate.rootScope)) {
                //checking for the first time for marking methods.
                //goes to the classDec scope to look for methodDec

                //if we reach to the JythonClass scope
                if (tempScope.getName() != null) {
                    for (Map.Entry<String, Symbol> entry : tempScope.getSymbolTable().getEntries().entrySet()) {
                        Matcher m = Pattern.compile(ctx.method_call().children.get(0).getText() + "@" + "([\\w+]*)").matcher(entry.getKey());

                        if (ctx.method_call().args().explist() == null) {
                            if (entry.getValue() instanceof JythonMethod &&
                                    ((JythonMethod) entry.getValue()).getParameters().size() == 0 &&
                                    m.matches()) {
                                found = true;
                                break;
                            }
                        } else {
                            if (entry.getValue() instanceof JythonMethod &&
                                    ((JythonMethod) entry.getValue()).getParameters().size() ==
                                            ctx.method_call().args().explist().expression().size() &&
                                    m.matches()) {
                                found = true;
                                break;
                            }
                        }
                    }
                    break;
                }
                tempScope = tempScope.getParentScope();
            }

            if (found) {
                //for several method call of the same name we add line number to make them individual
                //if it is already been defined mark it for true
                scope.getSymbolTable().getEntries().put(ctx.method_call().children.get(0).getText() + "@" + ctx.start.getLine(),
                        new JythonMethod(ctx.method_call().args().explist().expression().size(), ctx.start.getLine(), true));
            } else
            //else mark it for false
            {
                if (ctx.method_call().args().explist() != null)
                    scope.getSymbolTable().getEntries().put(ctx.method_call().children.get(0).getText() + "@" + ctx.start.getLine(),
                            new JythonMethod(ctx.method_call().args().explist().expression().size(), ctx.start.getLine(), false));
                else
                    scope.getSymbolTable().getEntries().put(ctx.method_call().children.get(0).getText() + "@" + ctx.start.getLine(),
                            new JythonMethod(0, ctx.start.getLine(), false));
            }
        }
    }


    @Override
    public void exitStatment(JythonParser.StatmentContext ctx) {
        super.exitStatment(ctx);
    }

    @Override
    public void enterReturn_statment(JythonParser.Return_statmentContext ctx) {
        super.enterReturn_statment(ctx);
    }

    @Override
    public void exitReturn_statment(JythonParser.Return_statmentContext ctx) {
        super.exitReturn_statment(ctx);
    }

    @Override
    public void enterCondition_list(JythonParser.Condition_listContext ctx) {
        //Error 220

        boolean conditionIsBoolean = false;

        for (JythonParser.ExpressionContext exp : ctx.expression()) {
            if (exp.relation_operators() != null || exp.eq_neq() != null) {
                conditionIsBoolean = true;
            }
            if (exp.rightExp() != null) {
                if (exp.rightExp().bool() != null) {
                    conditionIsBoolean = true;
                } else if (exp.rightExp().leftExp() != null) {
                    if (exp.rightExp().leftExp().methodName != null) {

                        String methodName = exp.rightExp().leftExp().methodName.getText();
                        Scope tempScope = scope;

                        while (tempScope.getParentScope() != null && !conditionIsBoolean) {

                            for (Map.Entry<String, Symbol> entry : tempScope.getSymbolTable().getEntries().entrySet()) {
                                Matcher m = Pattern.compile(methodName + "@" + "([\\w+]*)").matcher(entry.getKey());

                                if (m.matches() &&
                                        entry.getValue() instanceof JythonMethod &&
                                        ((JythonMethod) entry.getValue()).getReturnType().equals("bool") &&
                                        ((JythonMethod) entry.getValue()).getParameters().size() ==
                                                exp.rightExp().leftExp().args().explist().expression().size()) {
                                    conditionIsBoolean = true;
                                }
                            }
                            tempScope = tempScope.getParentScope();
                        }
                    } else if (exp.rightExp().leftExp().ID() != null) {
                        String varName = exp.rightExp().leftExp().varName.getText();
                        Scope tempScope = scope;

                        while (tempScope.getParentScope() != null && !conditionIsBoolean) {

                            for (Map.Entry<String, Symbol> entry : tempScope.getSymbolTable().getEntries().entrySet()) {
                                Matcher m = Pattern.compile(varName).matcher(entry.getKey());

                                if (m.matches() &&
                                        entry.getValue() instanceof JythonVariable &&
                                        ((JythonVariable) entry.getValue()).getType().equals("bool")) {
                                    conditionIsBoolean = true;
                                }
                            }
                            tempScope = tempScope.getParentScope();
                        }

                    }

                }
            }
        }
        if (!conditionIsBoolean) {
            System.out.println("Error220: in line " + ctx.start.getLine() + ", Condition type must be Boolean.");
        }
    }


    @Override
    public void exitCondition_list(JythonParser.Condition_listContext ctx) {
        super.exitCondition_list(ctx);
    }

    @Override
    public void enterWhile_statment(JythonParser.While_statmentContext ctx) {
        Scope whileScope = new Scope(scope);
        //setting child
        scope.addToChildrenScope(whileScope);
        scope = whileScope;

    }

    @Override
    public void exitWhile_statment(JythonParser.While_statmentContext ctx) {
        scope = scope.getParentScope();
    }

    @Override
    public void enterIf_else_statment(JythonParser.If_else_statmentContext ctx) {
        Scope ifElseScope = new Scope(scope);
        //setting child
        scope.addToChildrenScope(ifElseScope);
        scope = ifElseScope;
    }

    @Override
    public void exitIf_else_statment(JythonParser.If_else_statmentContext ctx) {
        scope = scope.getParentScope();
    }

    @Override
    public void enterPrint_statment(JythonParser.Print_statmentContext ctx) {

//        Scope tempScope = scope;
//        Scope tempScope2 = scope;
//
//
////            System.out.println(findExpressionType(ctx.expression().expression(0), tempScope2));
//
//            if (!(findExpressionType(ctx.expression(), tempScope2).equals("int") ||
//                    findExpressionType(ctx.expression(), tempScope2).equals("float") ||
//                    findExpressionType(ctx.expression(), tempScope2).equals("bool") ||
//                    findExpressionType(ctx.expression(), tempScope2).equals("string"))) {
//
//                System.out.println("Error270: in line " + ctx.start.getLine() + ": Type of parameter of print built-in" +
//                        "function must be integer , string ,float or boolean");
//
//            }


    }

    @Override
    public void exitPrint_statment(JythonParser.Print_statmentContext ctx) {
        super.exitPrint_statment(ctx);
    }

    @Override
    public void enterFor_statment(JythonParser.For_statmentContext ctx) {
        super.enterFor_statment(ctx);
    }

    @Override
    public void exitFor_statment(JythonParser.For_statmentContext ctx) {
        super.exitFor_statment(ctx);
    }

    @Override
    public void enterMethod_call(JythonParser.Method_callContext ctx) {
        // Error 240

        String methodName = ctx.ID().getText();
        int methodArgsSize = ctx.args().explist() == null ? 0: ctx.args().explist().expression().size();
        Scope tempScope = scope;
        Scope tempScope2 = scope;

        while (tempScope.getParentScope() != null) {

            for (Map.Entry <String, Symbol> entry : tempScope.getSymbolTable().getEntries().entrySet()) {
                Matcher m = Pattern.compile(methodName + "@" + "([\\w+]*)").matcher(entry.getKey());


                if (m.matches() &&
                        entry.getValue() instanceof JythonMethod &&
                        ((JythonMethod) entry.getValue()).getParameters() != null) {
                    if (((JythonMethod) entry.getValue()).getParameters().size() == methodArgsSize) {

                        if (methodArgsSize > 0 && ((JythonMethod) entry.getValue()).getParameters() != null) {
                            Iterator<JythonVariable> lookupVars = ((JythonMethod) entry.getValue()).getParameters().iterator();
                            Iterator<JythonParser.ExpressionContext> methodVars = ctx.args().explist().expression().iterator();

                            while (lookupVars.hasNext() && methodVars.hasNext()) {
                                JythonVariable lookupVar = lookupVars.next();
                                JythonParser.ExpressionContext methodVar = methodVars.next();

                                if (!lookupVar.getType().equals(findExpressionType(methodVar, tempScope2))) {
                                    System.out.println("Error240: in line " + ctx.start.getLine() + ", Mismatch arguments");
                                }

                            }
                        }
                    }
                    else {
                        System.out.println("Error240: in line " + ctx.start.getLine() + ", Mismatch arguments");

                    }


                }
            }
            tempScope = tempScope.getParentScope();
        }

    }



    public String findExpressionType(JythonParser.ExpressionContext expCtx, Scope searchScope){

        return ExpressionNode.findExpressionType(new ExpressionNode(expCtx, searchScope));
    }


    @Override
    public void exitMethod_call(JythonParser.Method_callContext ctx) {
        super.exitMethod_call(ctx);
    }

    @Override
    public void enterAssignment(JythonParser.AssignmentContext ctx) {

        //because of the way Jython.g4 has been defined it must be checked here again for phase 2 errors
        if (ctx.varDec() != null) {
            AtomicBoolean found = new AtomicBoolean(false);
            Scope tempScope = scope;

            while (tempScope.getParentScope().getParentScope() != null) {
                tempScope.getSymbolTable().getEntries().forEach((name, entry) -> {
                    if (name.equals(ctx.varDec().varName.getText()) && entry instanceof JythonVariable &&
                            (((JythonVariable) entry).getType().equals(ctx.varDec().type().getText()))) {
                        System.out.println("Error103: in line " + ctx.start.getLine() + ", " + ctx.varDec().varName.getText() +
                                " has been defined already in current scope"
                        );
                        found.set(true);
                    }
                });
                tempScope = tempScope.getParentScope();

            }


            //checking for undefined classes for variable declaration
            boolean isJType = false;
            boolean classExists = false;
            for (String jType : jythonTypes) {
                if (ctx.varDec().type().getText().equals(jType)) isJType = true;
            }
            if (!isJType) {
                for (String type : importedClasses) {
                    if (type.equals(ctx.varDec().type().getText())) {
                        classExists = true;
                    }
                }
            }
            //error 106
            if (!classExists && !isJType)
                System.out.println("Error106: in line " + ctx.start.getLine() + " ,cannot find class " + ctx.varDec().type().getText());


            if (!found.get())
                scope.getSymbolTable().getEntries().put(ctx.varDec().varName.getText() + "@" + ctx.start.getLine(), new JythonVariable(ctx.varDec().varName.getText(), ctx.varDec().type().getText(), ctx.start.getLine(), false));
            else
                scope.getSymbolTable().getEntries().put(ctx.varDec().varName.getText() + "@" + ctx.start.getLine(), new JythonVariable(ctx.varDec().varName.getText(), ctx.varDec().type().getText(), ctx.start.getLine(), true));

        }

//        if(ctx.leftExp() != null){
//            return;
//        }
//
//        if(ctx.expression() != null){
//            return;
//        }

        //TODO: if the method used is defined later, it won't be detected
        if (ctx.arrayDec() != null) {

            AtomicBoolean found = new AtomicBoolean(false);
            Scope tempScope = scope;

            while (tempScope.getName() == null) {
                tempScope.getSymbolTable().getEntries().forEach((name, entry) -> {
                    if (name.equals(ctx.arrayDec().arrayName.getText()) && entry instanceof JythonVariable &&
                            (((JythonVariable) entry).getType().equals(ctx.arrayDec().type().getText()))) {
                        System.out.println("Error103: in line " + ctx.start.getLine() + ", " + ctx.arrayDec().arrayName.getText() +
                                " has been defined already in current scope"
                        );
                        found.set(true);
                    }
                });
                tempScope = tempScope.getParentScope();
            }

            //checking for undefined classes for variable declaration
            boolean isJType = false;
            boolean classExists = false;
            for (String jType : jythonTypes) {
                if (ctx.arrayDec().type().getText().equals(jType)) isJType = true;
            }
            if (!isJType) {
                for (String type : importedClasses) {
                    if (type.equals(ctx.arrayDec().type().getText())) {
                        classExists = true;
                    }
                }
            }
            //error 106
            if (!classExists && !isJType)
                System.out.println("Error106: in line " + ctx.start.getLine() + " ,cannot find class " + ctx.arrayDec().type().getText());

            if (!found.get())
                scope.getSymbolTable().getEntries().put(ctx.arrayDec().arrayName.getText() + "@" + ctx.start.getLine(), new JythonVariable(ctx.arrayDec().arrayName.getText(), ctx.arrayDec().type().getText(), ctx.start.getLine(), false));
            else
                scope.getSymbolTable().getEntries().put(ctx.arrayDec().arrayName.getText() + "@" + ctx.start.getLine(), new JythonVariable(ctx.arrayDec().arrayName.getText(), ctx.arrayDec().type().getText(), ctx.start.getLine(), true));

            //Error 210
            boolean found1 = false;

            //Array initialised with a number
            if (ctx.expression().rightExp().INTEGER() != null) {
                found1 = true;
            }
            //Array initialised with a method
            else if (ctx.expression().rightExp().leftExp().methodName != null) {
                String methodName = ctx.expression().rightExp().leftExp().methodName.getText();
                tempScope = scope;

                while (tempScope.getParentScope() != null && !found1) {

                    for (Map.Entry<String, Symbol> entry : tempScope.getSymbolTable().getEntries().entrySet()) {
                        Matcher m = Pattern.compile(methodName + "@" + "([\\w+]*)").matcher(entry.getKey());

                        if (m.matches() &&
                                entry.getValue() instanceof JythonMethod &&
                                ((JythonMethod) entry.getValue()).getReturnType().equals("int") &&
                                ((JythonMethod) entry.getValue()).getParameters().size() ==
                                        ctx.expression().rightExp().leftExp().args().explist().expression().size()) {
                            found1 = true;
                        }
                    }
                    tempScope = tempScope.getParentScope();
                }
            }

            //Array initialised with a variable
            else if (ctx.expression().rightExp().leftExp().ID() != null) {
                String varName = ctx.expression().rightExp().leftExp().varName.getText();
                tempScope = scope;

                while (tempScope.getParentScope() != null && !found1) {

                    for (Map.Entry<String, Symbol> entry : tempScope.getSymbolTable().getEntries().entrySet()) {
                        Matcher m = Pattern.compile(varName).matcher(entry.getKey());

                        if (m.matches() &&
                                entry.getValue() instanceof JythonVariable &&
                                ((JythonVariable) entry.getValue()).getType().equals("int")) {
                            found1 = true;
                        }
                    }
                    tempScope = tempScope.getParentScope();
                }
            }
            if (!found1)
                System.out.println("Error210: in line " + ctx.start.getLine() + ", Size of an array must be of type integer");
        }


//        if (ctx.leftExp() != null && ctx.leftExp().varName != null){
//            String varName = ctx.leftExp().varName.getText();
//            Scope tempScope = scope;
//            Scope tempScope2 = scope;
//
//            while (tempScope.getParentScope() != null) {
//
//                for (Map.Entry<String, Symbol> entry : tempScope.getSymbolTable().getEntries().entrySet()) {
//                    Matcher m = Pattern.compile(varName).matcher(entry.getKey());
//                    Matcher m2 = Pattern.compile(varName + "@" + "([\\w+]*)").matcher(entry.getKey());
//
//                    if ((m.matches() || m2.matches()) &&
//                            entry.getValue() instanceof JythonVariable) {
//                        if (!((JythonVariable) entry.getValue()).getType().equals(findExpressionType(ctx.expression(), tempScope2))) {
//                            System.out.println("Error250: in line " + ctx.start.getLine() + ", Incompatible types: " + findExpressionType(ctx.expression(), tempScope2) + " can not be converted to " + ((JythonVariable) entry.getValue()).getType());
//                        }
//                    }
//                }
//                tempScope = tempScope.getParentScope();
//            }
//        }


    }

    @Override
    public void exitAssignment(JythonParser.AssignmentContext ctx) {
        super.exitAssignment(ctx);
    }

    @Override
    public void enterExpression(JythonParser.ExpressionContext ctx) {

//        if (ctx.rightExp() == null && ctx.eq_neq() == null) {
//            String exp1 = null;
//            String exp2 = null;
//
//            JythonParser.ExpressionContext tempCtx = ctx;
//            while (tempCtx.expression(0).expression() != null && tempCtx.expression(1).expression() != null){
//
//            }
//
//            System.out.println("Error200: " +"in line " + tempCtx.start.getLine() +", Parameters of relation operator are not int or float");
//
//            }
//
//
    }

//    public void varIsNumber(String name){}        related to enterExpression


    @Override
    public void exitExpression(JythonParser.ExpressionContext ctx) {
        super.exitExpression(ctx);
    }

    @Override
    public void enterRightExp(JythonParser.RightExpContext ctx) {
        super.enterRightExp(ctx);
    }

    @Override
    public void exitRightExp(JythonParser.RightExpContext ctx) {
        super.exitRightExp(ctx);
    }

    @Override
    public void enterLeftExp(JythonParser.LeftExpContext ctx) {
        super.enterLeftExp(ctx);

        //checking self. that has been defined in the class
        //Error 108
        Scope tempScope = scope;
        boolean notDef = true;

        if (ctx.children.get(0).getText().equals("self")) {
            ctx.leftExp().varName.getText();

            while (tempScope.getName() == null) {
                tempScope = tempScope.getParentScope();
            }
            for (Map.Entry<String, Symbol> entry : tempScope.getSymbolTable().getEntries().entrySet()) {
                if (ctx.leftExp().varName.getText().equals(entry.getValue().getName()) && entry.getValue() instanceof JythonVariable) {
                    notDef = false;
                    break;
                }
            }
            if (notDef) {
                System.out.println("Error 108: in line " + ctx.start.getLine() + ", Can not find Variable " + ctx.leftExp().varName.getText());
            }
        } else {
            notDef = false;
            //check for params in methodCall & in assignments and other conditions
            while (tempScope.getName() == null) {
                for (Map.Entry<String, Symbol> entry : tempScope.getSymbolTable().getEntries().entrySet()) {

                    if (ctx.methodName == null && ctx.varName == null) {
                        break;
                    }
                    if (ctx.varName == null) {
                        break;
                    } else if (ctx.methodName == null) {
                        notDef = true;
                        if (ctx.varName.getText().equals(entry.getValue().getName()) && !entry.getKey().contains("@") && entry.getValue() instanceof JythonVariable) {
                            notDef = false;
                            break;
                        }
                    } else {
                        break;
                    }
                }
                tempScope = tempScope.getParentScope();

            }
            //check in the SingleClassListener for further considerations

            if (notDef) {
                scope.getSymbolTable().getEntries().put(ctx.varName.getText() + "@@" + ctx.start.getLine(),
                        new JythonVariable(ctx.varName.getText(), null, ctx.start.getLine(), false));
            }
        }

    }

    @Override
    public void exitLeftExp(JythonParser.LeftExpContext ctx) {
        super.exitLeftExp(ctx);
    }

    @Override
    public void enterArgs(JythonParser.ArgsContext ctx) {
        super.enterArgs(ctx);
    }

    @Override
    public void exitArgs(JythonParser.ArgsContext ctx) {
        super.exitArgs(ctx);
    }

    @Override
    public void enterExplist(JythonParser.ExplistContext ctx) {
        super.enterExplist(ctx);
    }

    @Override
    public void exitExplist(JythonParser.ExplistContext ctx) {
        super.exitExplist(ctx);
    }

    @Override
    public void enterAssignment_operators(JythonParser.Assignment_operatorsContext ctx) {
        super.enterAssignment_operators(ctx);
    }

    @Override
    public void exitAssignment_operators(JythonParser.Assignment_operatorsContext ctx) {
        super.exitAssignment_operators(ctx);
    }

    @Override
    public void enterEq_neq(JythonParser.Eq_neqContext ctx) {
        super.enterEq_neq(ctx);
    }

    @Override
    public void exitEq_neq(JythonParser.Eq_neqContext ctx) {
        super.exitEq_neq(ctx);
    }

    @Override
    public void enterRelation_operators(JythonParser.Relation_operatorsContext ctx) {
        super.enterRelation_operators(ctx);
    }

    @Override
    public void exitRelation_operators(JythonParser.Relation_operatorsContext ctx) {
        super.exitRelation_operators(ctx);
    }

    @Override
    public void enterAdd_sub(JythonParser.Add_subContext ctx) {
        super.enterAdd_sub(ctx);
    }

    @Override
    public void exitAdd_sub(JythonParser.Add_subContext ctx) {
        super.exitAdd_sub(ctx);
    }

    @Override
    public void enterMult_mod_div(JythonParser.Mult_mod_divContext ctx) {
        super.enterMult_mod_div(ctx);
    }

    @Override
    public void exitMult_mod_div(JythonParser.Mult_mod_divContext ctx) {
        super.exitMult_mod_div(ctx);
    }

    @Override
    public void enterType(JythonParser.TypeContext ctx) {
        super.enterType(ctx);
    }

    @Override
    public void exitType(JythonParser.TypeContext ctx) {
        super.exitType(ctx);
    }

    @Override
    public void enterJythonType(JythonParser.JythonTypeContext ctx) {
        super.enterJythonType(ctx);
    }

    @Override
    public void exitJythonType(JythonParser.JythonTypeContext ctx) {
        super.exitJythonType(ctx);
    }

    @Override
    public void enterEveryRule(ParserRuleContext ctx) {
        super.enterEveryRule(ctx);
    }

    @Override
    public void exitEveryRule(ParserRuleContext ctx) {
        super.exitEveryRule(ctx);
    }

    @Override
    public void visitTerminal(TerminalNode node) {
        super.visitTerminal(node);
    }

    @Override
    public void visitErrorNode(ErrorNode node) {
        super.visitErrorNode(node);
    }
}