import Classes.JythonClass;
import Classes.Kind;

public class ClassListener extends JythonBaseListener {

    private Scope scope = new Scope(Translate.rootScope);

    @Override
    public void enterClassDec(JythonParser.ClassDecContext ctx) {
        if (Translate.rootScope.getSymbolTable().getEntries().containsKey(ctx.className.getText()))
        {
            System.out.println("Error101: in line "
                    + ctx.start.getLine() + ", class \""
                    + ctx.className.getText()
                    + "\" has been defined already");
        }
        else if (ctx.superClassName != null)
        {
            scope.setName(ctx.className.getText());
            Translate.rootScope.addToChildrenScope(scope);
            Translate.rootScope.getSymbolTable().getEntries().put(ctx.className.getText(), new JythonClass(ctx.className.getText(), ctx.superClassName.getText(),  Kind.CLASS, ctx.start.getLine()));
        }
        else
        {
            scope.setName(ctx.className.getText());
            Translate.rootScope.addToChildrenScope(scope);
            Translate.rootScope.getSymbolTable().getEntries().put(ctx.className.getText(), new JythonClass(ctx.className.getText(), Kind.CLASS, ctx.start.getLine()));
        }
    }
}
