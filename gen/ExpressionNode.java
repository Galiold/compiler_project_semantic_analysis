import Classes.JythonMethod;
import Classes.JythonVariable;
import Classes.Symbol;

import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by A. Goldani on 2019-06-30.
 * Student ID: 9512762107
 * compiler_project_semantic_analysis
 */

public class ExpressionNode {

    private JythonParser.ExpressionContext context;
    private ExpressionNode leftExpression, rightExpression;
    private Scope searchScope;

    private String type = null;


    public ExpressionNode(JythonParser.ExpressionContext context, Scope scope) {
        this.context = context;
        this.searchScope = scope;
        if (context.expression() != null){
            if (context.expression(0) != null){
                leftExpression = new ExpressionNode(context.expression(0), searchScope);
            }
            if (context.expression(1) != null){
                rightExpression = new ExpressionNode(context.expression(1), searchScope);
            }
        }
    }

    public JythonParser.ExpressionContext getContext() {
        return context;
    }

    public void setContext(JythonParser.ExpressionContext context) {
        this.context = context;
    }

    public ExpressionNode getLeftExpression() {
        return leftExpression;
    }

    public void setLeftExpression(ExpressionNode leftExpression) {
        this.leftExpression = leftExpression;
    }

    public ExpressionNode getRightExpression() {
        return rightExpression;
    }

    public void setRightExpression(ExpressionNode rightExpression) {
        this.rightExpression = rightExpression;
    }

    public Scope getSearchScope() {
        return searchScope;
    }

    public void setSearchScope(Scope searchScope) {
        this.searchScope = searchScope;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static String findExpressionType(ExpressionNode expression){

        if (expression.context.rightExp() == null){
            if (expression.context.add_sub() != null || expression.context.mult_mod_div() != null) {
                if (findExpressionType(expression.leftExpression).equals("int") && findExpressionType(expression.rightExpression).equals("int")) {
                    expression.setType("int");
                }
                else if (findExpressionType(expression.leftExpression).equals("float") && findExpressionType(expression.rightExpression).equals("float")) {
                    expression.setType("float");
                }
                else if ((findExpressionType(expression.leftExpression).equals("int") && findExpressionType(expression.rightExpression).equals("float")) ||
                        (findExpressionType(expression.leftExpression).equals("float") && findExpressionType(expression.rightExpression).equals("int"))) {
                    expression.setType("float");
                }
                else if (findExpressionType(expression.leftExpression).equals("string") || findExpressionType(expression.rightExpression).equals("string")) {
                    System.out.println("Error280 : in line " + expression.context.start.getLine() + ", operation not defined on these types");
                }
                else if (findExpressionType(expression.leftExpression).equals("bool") || findExpressionType(expression.rightExpression).equals("bool")) {
                    System.out.println("Error280 : in line " + expression.context.start.getLine() + ", operation not defined on these types");
                }
            }
            else if (expression.context.eq_neq() != null){
                if (findExpressionType(expression.leftExpression).equals("int") && findExpressionType(expression.rightExpression).equals("int")) {
                    expression.setType("bool");
                }
                else if (findExpressionType(expression.leftExpression).equals("float") && findExpressionType(expression.rightExpression).equals("float")) {
                    expression.setType("bool");
                }
                else if ((findExpressionType(expression.leftExpression).equals("int") && findExpressionType(expression.rightExpression).equals("float")) ||
                        (findExpressionType(expression.leftExpression).equals("float") && findExpressionType(expression.rightExpression).equals("int"))) {
                    expression.setType("bool");
                }
                else if (findExpressionType(expression.leftExpression).equals("string") && findExpressionType(expression.rightExpression).equals("string")) {
                    expression.setType("bool");
                }
                else if (findExpressionType(expression.leftExpression).equals("bool") && findExpressionType(expression.rightExpression).equals("bool")) {
                    expression.setType("bool");
                }
                else {
                    System.out.println("Error280 : in line " + expression.context.start.getLine() + ", operation not defined on these types");
                }
            }
            else if (expression.context.relation_operators() != null) {
                if (findExpressionType(expression.leftExpression).equals("int") && findExpressionType(expression.rightExpression).equals("int")) {
                    expression.setType("bool");
                }
                else if (findExpressionType(expression.leftExpression).equals("float") && findExpressionType(expression.rightExpression).equals("float")) {
                    expression.setType("bool");
                }
                else if ((findExpressionType(expression.leftExpression).equals("int") && findExpressionType(expression.rightExpression).equals("float")) ||
                        (findExpressionType(expression.leftExpression).equals("float") && findExpressionType(expression.rightExpression).equals("int"))) {
                    expression.setType("bool");
                }
                else{
                    System.out.println("Error280 : in line " + expression.context.start.getLine() + ", operation not defined on these types");

                }
            }
        }

        else if (expression.context.rightExp() != null){


            if (expression.context.rightExp().leftExp() != null){

                if (expression.context.rightExp().leftExp().args() != null) {

                    String methodName = expression.context.rightExp().leftExp().ID().getText();
                    int methodArgsSize = expression.context.rightExp().leftExp().args().explist() == null ? 0 : expression.context.rightExp().leftExp().args().explist().expression().size();


                    while (expression.searchScope.getParentScope() != null) {

                        for (Map.Entry<String, Symbol> entry : expression.searchScope.getSymbolTable().getEntries().entrySet()) {
                            Matcher m = Pattern.compile(methodName + "@" + "([\\w+]*)").matcher(entry.getKey());


                            if (m.matches() &&
                                    entry.getValue() instanceof JythonMethod &&
                                    ((JythonMethod) entry.getValue()).getParameters() != null) {
                                if (((JythonMethod) entry.getValue()).getParameters().size() == methodArgsSize) {

                                    expression.setType(((JythonMethod) entry.getValue()).getReturnType());

                                } else {
                                    System.out.println("Error240: in line " + expression.context.rightExp().leftExp().start.getLine() + ", Mismatch arguments");

                                }


                            }
                        }
                        expression.searchScope = expression.searchScope.getParentScope();
                    }
                }

                else if (expression.context.rightExp().leftExp().varName != null){
                    String varName = expression.context.rightExp().leftExp().varName.getText();
                    while (expression.searchScope.getParentScope() != null ) {

                        for (Map.Entry<String, Symbol> entry : expression.searchScope.getSymbolTable().getEntries().entrySet()) {
                            Matcher m = Pattern.compile(varName + "@" + "([\\w+]*)").matcher(entry.getKey());
                            Matcher m2 = Pattern.compile(varName).matcher(entry.getKey());

                            if ((m.matches() || m2.matches()) &&
                                    entry.getValue() instanceof JythonVariable) {
                                expression.setType(((JythonVariable) entry.getValue()).getType());
                            }
                        }
                        expression.searchScope = expression.searchScope.getParentScope();
                    }
                }

            }

            if (expression.context.rightExp().bool() != null) {
                expression.setType("bool");
            }
            else if (expression.context.rightExp().FLOAT() != null){
                expression.setType("float");
            }
            else if (expression.context.rightExp().INTEGER() != null){
                    expression.setType("int");
            }
            else if (expression.context.rightExp().STRING() != null){
                expression.setType("string");
            }
        }

        return expression.getType();
    }
}
