package Classes;

public class JythonVariable extends Symbol {
    private String type;


    public JythonVariable(String name,String type, int defLine, boolean resolved){
        this.setName(name);
        this.setType(type);
        this.setDefLine(defLine);
        setResolved(resolved);
    }

    public JythonVariable(String name,String type, int defLine){
        this.setName(name);
        this.setType(type);
        this.setDefLine(defLine);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
