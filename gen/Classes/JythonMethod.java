package Classes;

import java.util.ArrayList;

public class JythonMethod extends Symbol {
    private ArrayList<JythonVariable> parameters;
    private String returnType;
    private int sizeOfParameters;



    public JythonMethod(ArrayList<JythonVariable> parameters, String returnType, int line,boolean resolved){
        setReturnType(returnType);
        setParameters(parameters);
        setDefLine(line);
        setResolved(resolved);
    }
    public JythonMethod(int size, int line,boolean resolved){
        setSizeOfParameters(size);
        setResolved(resolved);
        setDefLine(line);
    }
    public ArrayList<JythonVariable> getParameters() {
        return parameters;
    }

    public void setParameters(ArrayList<JythonVariable> parameters) {
        this.parameters = parameters;

    }

    public String getReturnType() {
        return returnType;
    }

    public void setReturnType(String returnType) {
        this.returnType = returnType;
    }

    public int getSizeOfParameters() {
        return sizeOfParameters;
    }

    public void setSizeOfParameters(int sizeOfParameters) {
        this.sizeOfParameters = sizeOfParameters;
    }
}
