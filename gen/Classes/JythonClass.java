package Classes;

/**
 * Created by A. Goldani on 2019-05-17.
 * Student ID: 9512762107
 * compiler_project_semantic_analysis
 */

public class JythonClass extends Symbol {

    private JythonClass superClass = null;

    public JythonClass getSuperClass() {
        return superClass;
    }

    public void setSuperClass(JythonClass superClass) {
        this.superClass = superClass;
    }

    private String superClassName = "Symbol";

    public String getSuperClassName() {
        return superClassName;
    }
    public void setSuperClassName(String superClassName) {
        this.superClassName = superClassName;
    }

    public JythonClass(String name, Kind kind, int defLine) {
        this.setName(name);
        this.setKind(kind);
        this.setDefLine(defLine);
    }
    public JythonClass(String name, String superClassName, Kind kind, int defLine) {
        this.setName(name);
        this.setSuperClassName(superClassName);
        this.setKind(kind);
        this.setDefLine(defLine);
    }

    @Override
    public String toString() {
        return "{Kind: " + getKind()
                + ", Name: " + getName() +
                ", Extends from: " + getSuperClassName() +
                ", defined at line:" + getDefLine();
    }
}
