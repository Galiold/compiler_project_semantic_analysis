package Classes;

public abstract class Symbol {
    private String name;
    private Kind kind;
    private int defLine;
    private boolean isResolved;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Kind getKind() {
        return kind;
    }

    public void setKind(Kind kind) {
        this.kind = kind;
    }

    public int getDefLine() {
        return defLine;
    }

    public void setDefLine(int defLine) {
        this.defLine = defLine;
    }


    public boolean isResolved() {
        return isResolved;
    }

    public void setResolved(boolean resolved) {
        isResolved = resolved;
    }
}
